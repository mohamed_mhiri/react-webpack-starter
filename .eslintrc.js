module.exports = {
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "rules": {
        "react/jsx-filename-extension": 0,
        "no-use-before-define": 0
    },
    "parserOptions": {
        "ecmaVersion": 6
    },
    "env": {
        "node": true
    }
}